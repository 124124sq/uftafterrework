﻿'/////////////////////////////
'			Задание
'/////////////////////////////
'Описать класс, представляющий треугольник. 
'Предусмотреть методы для создания объектов, вычисления площади, периметра и точки пересечения медиан. 
'Описать свойства для получения состояния объекта.

'/////////////////////////////
'			Переменные
'/////////////////////////////

Option Explicit

Dim triangleClass

'/////////////////////////////
'		Работа с классом
'/////////////////////////////

'Создаём треугольник и задаём ему стороны
call CreateTriangle(triangleClass)
'Проверяем введённые данные
If (triangleClass.GetASide() + triangleClass.GetBSide() < triangleClass.GetCSide()) _
 or (triangleClass.GetASide() + triangleClass.GetCSide() < triangleClass.GetBSide()) _
 or (triangleClass.GetBSide() + triangleClass.GetCSide() < triangleClass.GetASide()) Then
	Msgbox "Такого треугольника не существует"
else
	'Если треугольник с такими сторонами может существовать, то считаем и выводим его данные
	msgbox "Стороны: " & triangleClass.GetASide()  & " " & triangleClass.GetBSide() & " " & triangleClass.GetCSide()
	msgbox "Периметр: " & triangleClass.Perimeter()
	msgbox "Площадь: " & triangleClass.Square()
	msgbox "Медианы:" & vbLF & "К стороне A " & triangleClass.MedianA() & vbLF & "К стороне B " & triangleClass.MedianB() & vbLF & "К стороне C " & triangleClass.MedianC()
	triangleClass.CenterOfTriangle' считаем координаты точки пересечения медиан треугольника
End If
