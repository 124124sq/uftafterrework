﻿'////////////////////////////////////////////
'			Задание
'////////////////////////////////////////////

'Дана любая строка на вход. 
'Заменить все ссылки и email на ***** (количество звездочек равно длине заменяемого фрагмента). 
'Примеры ссылок: www.site.com, http://site.com и т.п. 
'Решить двумя способами: с использованием регулярных выражений и без. Сравнить скорости работы.

'////////////////////////////////////////////
'			Переменные
'////////////////////////////////////////////

Option Explicit

Dim allWordsArray, inputString, outputString, regEx, match
Dim i 
inputString = "Привет BTV@inbox.com я http://site.com рад тебя видеть. Ты это, заходи на www.site.com хоть иногда"

'////////////////////////////////////////////
'		Поиск и замена без выражений
'////////////////////////////////////////////

inputString = InputBox ("Введите строку", "Поиск и замена", inputString)
allWordsArray = Split(inputString) 'делим на части
'Цикл от начала до конца введённого текста
For i  = 0 To UBound(allWordsArray)
	If not InStr(1, allWordsArray(i), ".com") = 0 Then 'Если в составной части был фрагмент .com тогда меняем его 
		allWordsArray(i) = Replace (allWordsArray(i), allWordsArray(i), string(len(allWordsArray(i)), "*"))
	End If
		outputString = outputString & allWordsArray(i) & " " 'Собираем строку обратно
Next
'Конец цикла
Msgbox outputString

'////////////////////////////////////////////
'Поиск и замена с использованием регулярного выражения
'////////////////////////////////////////////

inputString = InputBox ("Введите строку", "Поиск и замена с использованием регулярного выражения", inputString)
outputString = inputString
Set regEx = New RegExp ' Создать регулярное выражение
regEx.Pattern = "\S+\.\S+"	' Установить шаблон (\S+)-любые символы, кроме пробела.
regEx.IgnoreCase = True ' Установить нечувствительность к регистру
regEx.Global = True ' Установить глобальную применимость
'Цикл, для каждого совпадения с шаблоном в введённом тексте
For Each match in regEx.Execute(inputString) 
	outputString = Replace (outputString, match.Value, string(len(match.Value), "*"))  ' Выполнить поиск и замену. 
next
'Конец цикла
msgbox outputString
