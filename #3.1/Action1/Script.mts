﻿'//////////////////////////////////////
'				Задание
'//////////////////////////////////////

'Дан массив. Перемешать его элементы случайным образом так, чтобы каждый элемент оказался на новом месте.

'//////////////////////////////////////
'				Переменные
'//////////////////////////////////////

Option Explicit

const NUM = 9'Количество элементов массива
Dim numArray(), fileSystemObject, textFile, outputTextFile
Dim i, j, k
redim numArray(NUM)
outputTextFile = "C:\Users\" & Environment.Value("UserName") & "\Documents\UFT\Array.txt"

'//////////////////////////////////////
'Создаём массив и записываем его в файл
'//////////////////////////////////////

'Создаём объект файла для записи массива данных
outputTextFile = InputBox("Введите путь к файлу", "Путь к файлу", outputTextFile)
Set fileSystemObject = CreateObject("Scripting.FileSystemObject")
Set textFile = fileSystemObject.CreateTextFile(outputTextFile, 1)
'Цикл от первого до последнего элемента массива 
for i = 0 to NUM
	numArray(i) = i
	textFile.Write numArray(i) & " "
Next
'Конец цикла
textFile.Writeline

'//////////////////////////////////////
'Перемешиваем массив и записываем его в файл
'//////////////////////////////////////

Randomize
'Проходим по массиву и меняем положенеи значений массива
for i = 0 to NUM - 1
	j =int((NUM - i) * rnd + ( i + 1 ))'получаем рандомное Целое число от i до Num
	k = numArray(i)
	numArray(i) = numArray(j)
	numArray(j) = k
Next
'Конец цикла
'Запись массива в файл
'Цикл от первого до последнего элемента массива 
for i = 0 to NUM
	textFile.Write numArray(i) & " "
Next
'Конец цикла
textFile.Close
