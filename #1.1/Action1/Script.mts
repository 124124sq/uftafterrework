﻿'///////////////////////////////////////////
'			Задание
'///////////////////////////////////////////

'Дан файл (pwd.txt) с логинами и паролями.
'Найти 10 самых популярных паролей и поместить их в текстовый файл на рабочем столе, 
'в порядке их рейтинга (от самого популярного к менее);

'///////////////////////////////////////////
' 			Переменные
'///////////////////////////////////////////

Option Explicit

Dim passwordsDictionary, fileSystemObject, textFile, password, dictionaryArrayOfPasswords, inputFilePath, outputFilePath
password = " "
inputFilePath = "C:\Users\" & Environment.Value("UserName") & "\Documents\UFT\pwd.txt"
outputFilePath = "C:\Users\" & Environment.Value("UserName") & "\Desktop\1.txt"
Dim  i, j, k

'///////////////////////////////////////////
'Сохраниение всех паролей и частоты их появления в файле
'///////////////////////////////////////////

'Создаём словарь для записи паролей
Set passwordsDictionary = createObject("Scripting.Dictionary")
'Создаём объект файла для чтения данных
Set fileSystemObject = CreateObject("Scripting.FileSystemObject")
inputFilePath = InputBox("Введите путь к файлу с паролями", "Путь к файлу с паролями", inputFilePath)
Set textFile = fileSystemObject.OpenTextFile(inputFilePath, 1)
'Пока не конец файла
while not textFile.AtEndOfStream
	password = textFile.ReadLine
	password = Ltrim(mid(password, InStr(password, ";") + 1))
	'Проверяем наличие пароля в словаре
	if passwordsDictionary.Exists(password) Then 
		passwordsDictionary.Item(password) = passwordsDictionary.Item(password) + 1 'Если есть, то прибавляем единицу
	else
		passwordsDictionary.Add password, 1 'Если нету, то добавляем в словарь
	end if
	'Конец проверки
wend
'Конец цикла
textFile.Close
'закрываем файл обновляем переменные
password = ""

'///////////////////////////////////////////
'Поиск и запись 10 наиболее популярных паролей
'///////////////////////////////////////////

dictionaryArrayOfPasswords = passwordsDictionary.Keys 'получили все пароли
'Ищем 10 наболее часто встречающихся
for j = 1 to 10 
	'Ищем самый популярный из имеющихся
	for i = 0  to passwordsDictionary.Count - 1
		if passwordsDictionary.Item(k) < passwordsDictionary.Item(dictionaryArrayOfPasswords(i)) Then
			k = dictionaryArrayOfPasswords(i) 'Записываем наиболее популярный
		end if
	Next
	'Конец цикла
	password = password & k & "   " & passwordsDictionary.Item(k) & vbCrLf 'записываем результирующий ответ
	passwordsDictionary.remove(k) 'удаляем наиболее популярный
Next
'Конец цикла
'Записываем ответ
outputFilePath = InputBox("Введите путь для сохранения паролей", "Путь для сохранение", outputFilePath)
Set textFile = fileSystemObject.CreateTextFile(outputFilePath, 1) 
textFile.Write password
textFile.Close
'Закрываем запись
